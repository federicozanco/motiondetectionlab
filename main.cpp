/*
 * Taking ideas from https://blog.cedric.ws/opencv-simple-motion-detection
 */


#include "opencv2/opencv.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>

using namespace cv;
using namespace std;

#define PATH_PREFIX_DEFAULT "shots"

#define RESIZE_WIDTH 640
#define RESIZE_HEIGHT 512

#define ROI_X 150
#define ROI_Y 50
#define ROI_WIDTH 250
#define ROI_HEIGHT 450

#define MD_THRES_DEFAULT 8

#define CELL_THRES_DEFAULT 8
#define CELL_SIZE_DEFAULT 32

int shots_cursor = 0;
int thres_cursor = MD_THRES_DEFAULT;
int cell_thres_cursor = CELL_THRES_DEFAULT;
int cell_size_cursor = CELL_SIZE_DEFAULT;

vector<Mat> shots;

/**
* @function help
* @brief Indications of how to run this program and why is it for
*/
void help(string error)
{
    if (!error.empty())
    {
        cerr << endl;
        cerr << endl;
        cerr << "\t" << error << endl;
    }

    cout << endl;
    cout << "\t----------------------------------------" << endl;
    cout << "\t          Motion Detection Lab          " << endl;
    cout << "\t----------------------------------------" << endl;
    cout << "\t   Usage: ./motiondetectionlab <path>   " << endl;
    cout << endl;
}

void get_md_level(Mat motion)
{
    int rows = motion.rows / cell_size_cursor;
    int cols = motion.cols / cell_size_cursor;

    int counts[rows][cols];

    uchar *mptr = motion.ptr<uchar>(0);

    for (int i = 0; i < rows; i++)
        for (int j = 0; j < cols; j++)
            counts[i][j] = 0;

    int cur = 0;
    for (int i = 0; i < motion.rows; i++)
    {
        for (int j = 0; j < motion.cols; j++)
        {
            uchar p = mptr[cur++];

            int r = i / cell_size_cursor;
            int c = j / cell_size_cursor;

            if (r == rows || c == cols)
                continue;

            counts[r][c] += p > 0;
        }
    }

    double area = cell_size_cursor * cell_size_cursor;

    int activeCells = 0;

    for (int i = 0; i < rows; i++)
        for (int j = 0; j < cols; j++)
            activeCells += (int)((double ) counts[i][j] / area * 100.0) >= cell_thres_cursor;

    double level = (double ) activeCells / (double )(rows * cols) * 100.0;

    cout << endl;
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
            cout << "\t" << ((double ) counts[i][j] / (double ) area) * 100.0;
        cout << endl;
    }

    cout << "\tMD Level = " << level << endl;
}

/**
* @function onTrackbarChange
*/
void onTrackbarChange(int , void*)
{    
    Mat d1, d2, motion;

    // Erode kernel
    Mat kernel_ero = getStructuringElement(MORPH_RECT, Size(2,2));

    // Take a new image
    Mat prev_frame = shots[shots_cursor];
    Mat current_frame = shots[shots_cursor + 1];
    Mat next_frame = shots[shots_cursor + 2];

    // Calc differences between the images and do AND-operation
    // threshold image, low differences are ignored (ex. contrast change due to sunlight)
    absdiff(prev_frame, next_frame, d1);
    absdiff(next_frame, current_frame, d2);
    bitwise_and(d1, d2, motion);
    threshold(motion, motion, thres_cursor, 255, CV_THRESH_BINARY);

    //erode(motion, motion, kernel_ero);

    get_md_level(motion);

    imshow( "shot0", prev_frame);
    imshow( "shot1", current_frame);
    imshow( "shot2", next_frame);
    imshow( "md", motion);
}

/**
 * @brief check_dir
 * @param path
 * @return true if the directory exists
 */
bool check_dir(string path)
{
    struct stat statbuf;

    return stat(path.c_str(), &statbuf) != -1 && S_ISDIR(statbuf.st_mode);
}

vector<Mat> scan_dir(string path)
{
    vector<Mat> files;

    DIR *dp;

    struct dirent *dirp;

    if((dp  = opendir(path.c_str())) == NULL)
    {
        cout << "Error(" << errno << ") opening " << path << endl;
        return files;
    }

    vector<string> fullPaths;

    while ((dirp = readdir(dp)) != NULL)
    {
        if (strcmp(dirp->d_name, ".") != 0
                && strcmp(dirp->d_name, "..") != 0
                && strcmp(dirp->d_name, ".directory") != 0)
        {
            fullPaths.push_back(path + "/" + dirp->d_name);
        }
    }

    closedir(dp);

    if (fullPaths.size() > 0)
    {
        sort(fullPaths.begin(), fullPaths.end());

        for (int i = 0; i < (int)fullPaths.size(); i++)
        {
            Mat shot = imread(fullPaths[i]);
            cv::resize(shot, shot, Size(640, 512));
            Mat roi = shot(Rect(ROI_X, ROI_Y, ROI_WIDTH, ROI_HEIGHT)).clone();
            cvtColor(roi, roi, CV_RGB2GRAY);
            GaussianBlur(roi, roi, Size(3, 3), 2.0);
            files.push_back(roi);

            cout << "\tread " << fullPaths[i] << endl;
        }
    }

    return files;
}

/**
* @function main
*/
int main(int argc, char *argv[])
{
    string path = PATH_PREFIX_DEFAULT;

    if (argc > 1)
        path = *argv[1];

    if (!check_dir(path))
    {
        help("cannot open " + path);
        return -1;
    }

    shots = scan_dir(path);

    if(shots.empty())
    {
        help("no shots read");
        return -1;
    }

    namedWindow( "shot0", WINDOW_AUTOSIZE );
    namedWindow( "shot1", WINDOW_AUTOSIZE );
    namedWindow( "shot2", WINDOW_AUTOSIZE );
    namedWindow( "md", WINDOW_AUTOSIZE );

    createTrackbar( "shots", "md", &shots_cursor, shots.size() - 3, onTrackbarChange);
    createTrackbar( "thres", "md", &thres_cursor, 255, onTrackbarChange);
    createTrackbar( "cell size", "md", &cell_size_cursor, 255, onTrackbarChange);
    createTrackbar( "cell thres", "md", &cell_thres_cursor, 255, onTrackbarChange);

    onTrackbarChange(0, 0);

    waitKey(0);
    return 0;
}

